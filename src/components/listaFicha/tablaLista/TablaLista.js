import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faClipboardList, faFilePdf} from "@fortawesome/free-solid-svg-icons";
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import logolat from './../../../assets/img/logolat.png';
import $ from 'jquery';
window.jQuery = $;

class TablaLista extends React.Component {
    componentDidMount() {
        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $(document).ready(function(){
            $('#tablaPag').DataTable({
                language: {
                    processing: "Tratamiento en curso...",
                    search: "Buscar Ficha Tecnica &nbsp;:",
                    lengthMenu: "",
                    info: "_TOTAL_ Fichas Tecnicas",
                    infoEmpty: "No existen datos.",
                    infoFiltered: "(filtrado de _MAX_ elementos en total)",
                    infoPostFix: "",
                    loadingRecords: "Cargando...",
                    zeroRecords: "No se encontraron datos con tu busqueda",
                    emptyTable: "No hay datos disponibles en la tabla.",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": active para ordenar la columna en orden ascendente",
                        sortDescending: ": active para ordenar la columna en orden descendente"
                    }
                },
                scrollY: 280,
                lengthMenu: [ [5, -1], [5] ],
                searching: false,  
            });
        });
    }
    render(){
        return(
            <div style={{ backgroundColor:'#A46F1F'}} class="container-fluid vh-100">

                <div id="mostrar-nav" class="body"></div>
                    <nav class="nav-lat">
                        <div id="cerrar-nav"></div>
                        
                        <div><img src={logolat} class="logo"></img></div>
                        
                            <ul class="menu">
                                <li><a href="/#/home"><b>inicio</b></a></li>
                                <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                                <li><a href="#"><b>Servicios</b></a></li>
                            </ul>
                    </nav>
                    
                    <br/>
                <div id="divTabla" style={{ backgroundColor:'#E5E5E5'}} class="container px-1 py-5">
                    <div class="d-flex container text-center">
                        <div class="p-2 container">
                            <div class="">
                                <input type="text" class="form-control w-50" placeholder="Buscar Fichas Tecnicas" value=""/>
                            </div>
                        </div>
                        <div class="container-fluid ml-auto p-2">
                            <div class="botonNuevo">
                                <a href="/#/nuevaFicha">
                                <input  type="button" class="btn" style={{backgroundColor:'#696666', color:'#ffffff'}} value="Nueva Ficha Tecnica"></input></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container px-4">
                        <table class="table" id="tablaPag">
                            <thead class="table-dark text-center">
                                <tr>
                                    <th scope="col">CODIGO</th>
                                    <th scope="col">LINEA</th>
                                    <th scope="col">SERIE</th>
                                    <th scope="col">ALT. TACO</th>
                                    <th scope="col">COLOR</th>
                                    <th scope="col">ESTILO</th>
                                    <th scope="col">HORMA</th>
                                    <th scope="col">PLANTA</th>
                                    <th scope="col">OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td>1</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Reyna</td>
                                    <td>34 al 39</td>
                                    <td>10</td>
                                    <td>Hueso</td>
                                    <td>Reyna</td>
                                    <td>50740</td>
                                    <td>Letycita</td>
                                    <td>
                                        <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                        {" "}
                                        <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><br/>
                </div>
            </div>
        )
    }
}
export default TablaLista;