import React from 'react';
import login1 from './../../assets/img/login1.jpg';

class Login extends React.Component {
    render(){
        return(
           <div>
                <div class="row">
                    <div class="col-6" style={{backgroundColor:'#696666'}}>
                        <div>
                            <img src={login1} alt="Citeccal Empleados" class="img-fluid float-center vh-100"/>
                        </div>
                    </div>
                    <div class="col-6 flex-fill" style={{backgroundColor:'#696666'}}>
                        <div class="row vh-100 justify-content-center align-items-center">  
                            <div class="col-auto" >
                                <h1 style={{color:'#FFFFFF'}}><b>Website Administrativo</b></h1><br/>
                                <form class="">
                                    <div class="input-group p-2">
                                        <input type="text" name="email" placeholder="Usuario"  class="form-control" value=""/>
                                    </div>
                                        
                                    <div class="input-group p-2">
                                        <input type="password" name="password" placeholder="Contraseña"  class="form-control" value=""/>
                                    </div><br/>

                                    <div class="text-center">
                                        <a href="/#/home">
                                        <input type="button" class="btn w-75 rounded-pill" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Iniciar Sesión"></input></a>
                                    </div>  
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        )
    }
}
export default Login;

    

            